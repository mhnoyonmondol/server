<?php
    $host = "10.148.0.3";
    $port = 7788;
    set_time_limit(0);
    $sock = socket_create(AF_INET,SOCK_STREAM,0) or die("Could not create socket \n");
    $result = socket_bind($sock,$host,$port) or die("Could not bind socket params of $port and $host \n");

    $result = socket_listen($sock,3) or die("Could not set up socket listener \n");
    echo "Listening connections";
    class Chat{
        function readline(){
            return rtrim(fgets(STDIN));
        }
        function reply($client_message){
            $arr = array(
                "hi" => "Hm",
                "how are you" => "I am fine",
                "how old are you" => "Just 23 years old",
                "bye" => "Thank's for communicate",
            );
            $result = "Unknown question";
            if (isset($arr[$client_message])){
                $result = $arr[$client_message];
            }
            return $result;
        }
    }
    do{
        $accept = socket_accept($sock) or die("Could not accept incoming connections \n");
        $msg = socket_read($accept,1024) or die("Could not read input \n");

        $msg = trim($msg);
        echo "Client says: \t $msg \n\n";

        $chat = new Chat();
        $reply = $chat->reply($msg);
        echo "Enter replay: \t".$reply."\n\n";

        socket_write($accept,$reply,strlen($reply)) or die("Could not create output");
        socket_close($accept);
    }while(true);
    socket_close($sock);

