const express = require('express');
const socket = require('socket.io');
const cors = require('cors');
const app = express();
app.use(express.static('public'));
var string = "listening at http://localhost:4000";
const server = app.listen(4000,() => console.log(string));
const io = socket(server);
io.on("connection",socket => {
    console.log("Connected on Socket with Id: "+socket.id);
    socket.on("reg",function (data) {
        console.log(data);
    })
});